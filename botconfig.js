module.exports = {
  Admins: [""], //Admins of the bot
  ExpressServer: true, //If you wanted to make the website run or not
  DefaultPrefix: process.env.Prefix || "!a", //Default prefix, Server Admins can change the prefix
  Port: 3000, //Which port website gonna be hosted
  SupportServer: "https://discord.com/invite/J3S3Jugh6a", //Support Server Link
  Token: process.env.Token || "TOKEN", //Discord Bot Token
  ClientID: process.env.Discord_ClientID || "888251077026267176", //Discord Client ID
  ClientSecret: process.env.Discord_ClientSecret || "8VOIjCHkSBw2JMwrPhS6neXeCh_a9P1S", //Discord Client Secret
  Scopes: ["identify", "guilds", "applications.commands"], //Discord OAuth2 Scopes
  ServerDeafen: true, //If you want bot to stay deafened
  DefaultVolume: 100, //Sets the default volume of the bot, You can change this number anywhere from 1 to 100
  CallbackURL: "/api/callback", //Discord OAuth2 Callback URL
  "24/7": true, //If you want the bot to be stay in the vc 24/7
  CookieSecret: "Pikachu is cute", //A Secret like a password
  IconURL:
    "https://raw.githubusercontent.com/SudhanPlayz/Discord-MusicBot/master/assets/logo.gif", //URL of all embed author icons | Dont edit unless you dont need that Music CD Spining
  EmbedColor: "RANDOM", //Color of most embeds | Dont edit unless you want a specific color instead of a random one each time
  Permissions: 2205281600, //Bot Inviting Permissions
  Website: process.env.Website || "", //Website where it was hosted at includes http or https || Use "0.0.0.0" if you using Heroku
  
  Presence: {
    status: "online", // You can show online, idle, and dnd
    name: "!ahelp/哈囉～嫌疑犯們!", // The message shown
    type: "LISTENING", // PLAYING, WATCHING, LISTENING, STREAMING
  },

  //Lavalink
  Lavalink: {
    id: "Main",
    host: "lava.pumpdev.org",
    port: 3799, // The port that lavalink is listening to. This must be a number!
    pass: "pumpisfree",
    secure: false, // Set this to true if the lavalink uses SSL or you're hosting lavalink on repl.it
  },

  //Please go to https://developer.spotify.com/dashboard/
  Spotify: {
    ClientID: process.env.Spotify_ClientID || "06f7bc44fc3e4856a6a1650e6ffd1e83", //Spotify Client ID
    ClientSecret: process.env.Spotify_ClientSecret || "b788fc3f8f224f6abed472e9e8ce8742", //Spotify Client Secret
  },
};
