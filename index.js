const DiscordMusicBot = require("./structures/DiscordMusicBot");
const client = new DiscordMusicBot();

client.build();

client.on("message", async (message) => {
    // 如果訊息的內容是 '早安'
    //if (message.content.includes('笑死')) {
        // 則 Bot 回應 '歐嗨喲~今天也好好努力吧!!'
        //message.channel.send('笑鼠');
    //}
    // 如果訊息的內容是 '晚安'
    //if (message.content.includes('')) {
        // 則 Bot 回應 '今天的你辛苦了!! 晚安~'
        //message.channel.send('今天的你辛苦了!! 好好休息吧~');
    //}
    if (message.content.includes('的機率')) {
        // 則 Bot 回應 '機率!!'
        message.channel.send((Math.round(Math.random()*101)) + '%'); 
    }

  if (message.author.bot) return;
  if (!message.guild) return;
});

module.exports = client; 
